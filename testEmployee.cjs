const { dataId, groupData, PowerpuffData, entryId2, sortData, swapCompany, birthdate } = require("./employee.cjs")
let data = require("./data.json");


async function init(data) {
    try {
        const idData = await dataId(data);
        const groupResult = await groupData(data);
        const companyData = await PowerpuffData(data);
        const newData = await entryId2(data);
        const sortedData = await sortData(data);
        const swapData = await swapCompany(data);
        const birthData = await birthdate(data);
        console.log(birthData);
    }
    catch (err) {
        console.log(err);
    }
}
init(data);